<div class="row">
	<div class="col-12 col-s-12">
		
		<form id="Ftask" name="Ftask">
			<label class="w3-text-blue">Input T:</label>
      <input class="step-input w3-border" type="text" id="txtT" name="txtT" placeholder="T" value="1">
			<br><br>

			<label class="w3-text-blue">Input A:</label>
      <input class="step-input w3-border" type="text" id="txtA" name="txtA" placeholder="A" value="1">
			<br><br>

			<label class="w3-text-blue">Input B:</label>
      <input class="step-input w3-border" type="text" id="txtB" name="txtB" placeholder="B" value="10">
			<br><br>

			<label class="w3-text-blue">Input K:</label>
      <input class="step-input w3-border" type="text" id="txtK" name="txtK" placeholder="K" value="3">
			<br><br>

			<input type="button" id="save" name="save" value="Get Solution">
		</form>
		<br>

		<!--<div id="pagination" class="w3-center">
		</div>
		<div class="scrollable"> 
			<table id="tbl-patient" class="table-adminer" style="width: 100%">
				<thead>
					<tr>
						<th>Medical Records Id</th>
						<th>Name</th>
						<th>Address</th>
						<th>Birthday</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>-->

		Output :
		<div id="resftask" class="scrollable" style="overflow-y: scroll;max-height: 50px;">
		</div>
		<!--<br>-->
		
	</div>
</div>

<script type="text/javascript">
	$("#save").click(function () {
		var prm = 'keyword=firsttask&' + $('#Ftask').serialize(); 
		if (constraints()) {
			//console.log('ftask' + '-' + prm);
			_post('ftask', prm, 'ftask|resftask');
		}
		//resviews(prm);
	})

	function resftask(id, res) { //{"code":4,"task":{"result":3,"data":[3,6,9],"input":{"a":"1","b":"10","k":"3"}}}
		var str = 'Case ';
		str += res.task.input.t + ': ' + res.task.result;
		$('#' + id).append('<div>' + str + '</div>');
		$('#txtT').val(res.task.next);
	}
	function constraints() {
		var res = true;

		if (parseInt($('#txtT').val())<1 || parseInt($('#txtT').val())>parseInt(100)) {
			alert('Input T, more than 1 and less than 100');
			$('#txtT').focus();
			$('#txtT').select();
			res = false;
		} else if (parseInt($('#txtA').val())<1 || parseInt($('#txtA').val())>parseInt($('#txtB').val()) ) {
			alert('Input A, more than 1 and less than B' + $('#txtA').val());
			$('#txtA').focus();
			$('#txtA').select();
			res = false;
		} else if (parseInt($('#txtB').val())<parseInt($('#txtA').val()) || parseInt($('#txtB').val())>parseInt(10000)) {
			alert('Input B, more than A and less than 10000');
			$('#txtB').focus();
			$('#txtB').select();
			res = false;
		} else if (parseInt($('#txtK').val())<1 || parseInt($('#txtK').val())>parseInt(10000)) {
			alert('Input K, more than 1 and less than 10000');
			$('#txtK').focus();
			$('#txtK').select();
			res = false;
		}

	return res;
	}
</script>