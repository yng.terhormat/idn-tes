<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ConTaskF extends Controller {

	public function taskF(Request $request) {
		$prm = array();
		$prm['child'] = view('childs.taskF', $prm); //"Master Patients";
		return view('index-', $prm);
	}
	public function send(Request $request) {
		$keyword = $request->input('keyword');
		$res = array();
		$res['code'] = 2;
		switch ($keyword) {
			case 'firsttask':
				$res['task'] = $this->Ftask(['t' => $request->input('txtT'), 'a' => $request->input('txtA'), 'b' => $request->input('txtB'), 'k' => $request->input('txtK')]);
				$res['code'] = 4;
				break;
			
			default:

			break;
		}

	return $res;
	}
	public function Ftask($arr) {
		$res = array();
		$res['result'] = 0;
		for ($i=$arr['a']; $i < $arr['b']; $i++) {
			if ( ($i%$arr['k'])==0 ) {
				$res['result'] += 1;
				$res['data'][] = $i;
			}
		}
		$res['input'] = $arr;
		$res['next'] = $arr['t'] + 1;

	return $res;
	}
}
