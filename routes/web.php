<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	//return view('welcome');
	$arr = array();
	$arr['child'] = view('childs.home', $arr);
	return view('index-', $arr);
});/**/

Route::get('/ftask', 'ConTaskF@taskF');
Route::post('/ftask', 'ConTaskF@send');